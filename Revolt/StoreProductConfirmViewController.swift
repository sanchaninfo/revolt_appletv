/***
 **Module Name: ProductConfirmViewController.
 **File Name :  ProductConfirmViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Product Confirmation page.
 */

import UIKit
import Kingfisher

class StoreProductConfirmViewController: UIViewController {

    var largeImageUrl = String()
    var producttl,desc,price,quantity,tot,size : String!
    var color:UIColor!
    var userId = String()
    var productID = String()
    var colorCode = String()
    var billArray = NSArray()
    var totalPrice = String()
    var uuid,deviceId:String!
    var dataDict = NSDictionary()
    @IBOutlet weak var largeImage: UIImageView!
    @IBOutlet weak var selectClt: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descTitle: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var selectsize: UILabel!
    @IBOutlet weak var cnfBtn: UIButton!
    @IBOutlet weak var quan: UILabel!
    var billAddress = NSArray()
    var fromPlayLayer = Bool()
    override func viewDidLoad() {
       
        super.viewDidLoad()
      //  getaccountInfo()
        cnfBtn.layer.cornerRadius = 3.0
        largeImage.kf.setImage(with: URL(string: largeImageUrl))
        productTitle.text = producttl
        descTitle.text = desc
       // priceLbl.text = price
        quan.text = quantity
        let Qty = Int(quantity)!
        let tot1 = Float(tot)!
        let tot2 = (Float(Qty) * tot1)
        totalPrice = String(format: "%0.2f", tot2)
        
        selectsize.text = size
       // selectClt.layer.cornerRadius = min(selectClt.frame.size.height, selectClt.frame.size.width)/2.0
       // selectClt.clipsToBounds = true
        let attr = NSAttributedString(string: "$"  +  (String(format: "%0.2f", tot2)), attributes:[NSForegroundColorAttributeName:(UIColor.init(red: 200/255, green: 169/255, blue: 133/255, alpha: 1.0))])
        cnfBtn.setAttributedTitle(attr, for: .normal)
        selectClt.backgroundColor = color
        selectClt.layer.borderColor = focusColor
        selectClt.layer.borderWidth = 5.0
    }

    @IBAction func confirmBtn(_ sender: AnyObject) {
       // getFetchAddress()
        getaccountInfo()
    }
    func getFetchAddress()
    {
        let parameters = ["fetchAddressStore": ["user_id": userId]]
        ApiManager.sharedManager.postDataWithJson(url: kFetchAddress, parameters:parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSArray
                if JSON.count == 0
                {
                    self.gotoNoBillPage()
                }
                else
                {
                let addressDict = JSON.firstObject as! NSDictionary
                self.billArray = addressDict["address"] as! NSArray
                self.gotoBillPage()
                }
            }
            else
            {
                print("json error")
            }
        }
    }
    
    func gotoNoBillPage()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let NobillPage = storyBoard.instantiateViewController(withIdentifier: "NoBill") as! StoreNoBillViewController
        self.navigationController?.pushViewController(NobillPage, animated: true)
    }
    func gotoBillPage()
    {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let billPage = storyBoard.instantiateViewController(withIdentifier: "Billing") as! StoreBillingViewController
            billPage.largeImageUrl = self.largeImageUrl
            billPage.productName = self.producttl
            billPage.size = self.size
            billPage.color = self.selectClt.backgroundColor!
            billPage.quantity = self.quantity
            billPage.total = self.totalPrice
            billPage.productID = self.productID
            billPage.colorCode = self.colorCode
            billPage.billArray = self.billArray
            if dataDict.count != 0
            {
             billPage.accountDict = dataDict
             if fromPlayLayer
             {
              billPage.fromPlayLayer = true
            }
                else
             {
                billPage.fromPlayLayer = false
                }
             self.navigationController?.pushViewController(billPage, animated: true)
            }
            else
            {
                getaccountInfo()
            }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == self.cnfBtn
        {
            cnfBtn.layer.borderWidth = 5.0
            cnfBtn.layer.borderColor = focusColor
        }
    }
    
    func getaccountInfo()
    {
        let url = kAccountInfoUrl
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                if (dict["uuid_exist"] as! Bool) == false
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
                else
                {
                    self.getFetchAddress()
                    self.dataDict = dict
                }
               
            }
            else
            {
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
