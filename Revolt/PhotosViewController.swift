/***
 **Module Name:  PhotosViewController
 **File Name :  PhotosViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Enlarge Photo when click on from Landing page..
 */

import UIKit

class PhotosViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var leftdisclose: UIImageView!
    @IBOutlet weak var rightdisclose: UIImageView!
    @IBOutlet weak var photocollectionView: UICollectionView!
    var Img:UIImageView!
    var PhotoDict = [[String:Any]]()
    var ImageIndexpath = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        leftdisclose.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photocollectionView.scrollToItem(at: IndexPath(row: ImageIndexpath, section: 0) , at: .centeredHorizontally, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PhotoDict.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let Path = PhotoDict[indexPath.row] as NSDictionary
        (cell.viewWithTag(10) as! UIImageView).kf.indicatorType = .activity
        (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: (kPhotoBaseUrl + (Path["tv"] as! String))))
        return cell
    }
        func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            
            if let previousIndexPath = context.previouslyFocusedIndexPath,
                let cell = collectionView.cellForItem(at: previousIndexPath)
            {
                cell.transform = CGAffineTransform.identity
            }
            if let indexPath = context.nextFocusedIndexPath,
                let  _ = collectionView.cellForItem(at: indexPath)
            {
                switch indexPath.row {
                case 0:
                    self.leftdisclose.isHidden = true
                    break
                case (PhotoDict.count) - 1:
                    self.rightdisclose.isHidden = true
                    
                    break
                default:
                    self.rightdisclose.isHidden = false
                    self.leftdisclose.isHidden = false
                    break
                }
            }
            collectionView.endInteractiveMovement()
        }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let rightOffset:CGFloat = (photocollectionView.frame.size.width) * (CGFloat(PhotoDict.count - 1))
//        if scrollView.contentOffset.x == rightOffset
//        {
//            photocollectionView.scrollToItem(at: IndexPath(row: ImageIndexpath, section: 0) , at: .centeredHorizontally, animated: true)
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
